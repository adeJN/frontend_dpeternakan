jQuery(document).ready(function ($) {


    //FILBOX
    $('.thumbnail-img img').fillBox();

    //MENU-HOVER

    // Large devices <large desktops, 992px and up>
    if ($(window).width() > 992) {
        $('.has-dropdown, .has-sub-dropdown, .has-subs-dropdown').mouseenter(function () {
            var $this = $(this);

            $this.addClass('is-opened')

        }).mouseleave(function () {

            var $this = $(this);

            $this.removeClass('is-opened')

        });
    }

    // Medium devices <landscape phones, 576px and up>
    $(".has-dropdown > a").click(function () {
        if ($(this).parent().hasClass('is-opened')) {

            $(".has-dropdown.is-opened").removeClass("is-opened");

        } else {

            $(".has-dropdown.is-opened").removeClass("is-opened");

            if ($(this)) {

                $(this).parent().addClass("is-opened");
            }
        }
    });

    $(".has-sub-dropdown > a").click(function () {
        if ($(this).parent().hasClass('is-opened')) {

            $(".has-sub-dropdown.is-opened").removeClass("is-opened");

        } else {

            $(".has-sub-dropdown.is-opened").removeClass("is-opened");

            if ($(this)) {

                $(this).parent().addClass("is-opened");
            }
        }
    });

    $(".has-subs-dropdown > a").click(function () {
        if ($(this).parent().hasClass('is-opened')) {

            $(".has-subs-dropdown.is-opened").removeClass("is-opened");

        } else {

            $(".has-subs-dropdown.is-opened").removeClass("is-opened");

            if ($(this)) {

                $(this).parent().addClass("is-opened");
            }
        }
    });


    //BURGER-MENU
    $('.navigation-burger, .main-nav .btn-close').click(function () {
        navigationToggle();
    });

    function navigationToggle() {
        $('.navigation-burger').toggleClass('is-active');
        $('.menubar-center').toggleClass('bar-is-opened');
        $('body').toggleClass('scroll-lock');
        $(".has-dropdown.is-opened").removeClass("is-opened");
    }


    //SEARCH-NAV
    $('.search-nav, .search-wrap .btn-close').click(function () {
        searchToggle();
    });

    function searchToggle() {
        $('.search-nav').toggleClass('active');
        $('body').toggleClass('big-search');
        setTimeout(function () {
            if ($('body').hasClass('big-search')) {
                $('#search-header').focus();
            } else {
                $('#search-header').val('');
            }
        }, 300);

    }



    //BACK TOP
    var btn = $('#button');

    $(window).scroll(function () {
        if ($(window).scrollTop() > 500) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, '300');
    });




});