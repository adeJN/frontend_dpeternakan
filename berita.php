<?php include('general_header.php'); ?>

<body style=" background-color: #e5e5e5;">
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c5d8b22ddc43907"></script> -->
    <div id="page">
        <?php include('general_navbar.php'); ?>
        <main>
            <main>
                <div class="banner-breadcrumb">
                    <div class="container">
                        <div class="banner-content">
                            <div class="banner-content-text">
                                <div class="title-heading text-center">
                                    <h2>Berita Terkini</h2>
                                </div>
                            </div>
                            <nav class="breadcrumb">
                                <ul class="breadcrumb-list">
                                    <li><a href="">Beranda</a></li>
                                    <li class="current">
                                        Berita </li>
                                </ul>
                            </nav>

                        </div>
                    </div>
                    <div class="thumbnail-img">
                        <img src="assets/img/gallery.jpg">
                    </div>
                </div>
                <div class="box-wrap bg-grey-1 news">
                    <div class="container">
                        <div class="list-news">
                            <div class="row justify-content-center">

                                <div class="col-md-6 col-lg-6 col-xl-3">
                                    <a href="berita_detail.php" class="item-news">
                                        <div class="box-img">
                                            <div class="thumbnail-img">
                                                <img src="assets/img/berita/berita1.jpg">
                                            </div>
                                        </div>
                                        <div class="box-post">

                                            <h3 class="post-title" style="text-transform: uppercase;">
                                                <span>Perkumpulan para ternak</span>
                                            </h3>
                                            <div class="summary">
                                                <p><strong>DINAS &ndash;</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                            </div>
                                            <div class="post-date">
                                                November 02, 2021 </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xl-3">
                                    <a href="berita_detail.php" class="item-news">
                                        <div class="box-img">
                                            <div class="thumbnail-img">
                                                <img src="assets/img/berita/berita2.jpg">
                                            </div>
                                        </div>
                                        <div class="box-post">

                                            <h3 class="post-title" style="text-transform: uppercase;">
                                                <span>Ayam jago berjenis</span>
                                            </h3>
                                            <div class="summary">
                                                <p><strong>DINAS &ndash;</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                            </div>
                                            <div class="post-date">
                                                November 02, 2021 </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xl-3">
                                    <a href="berita_detail.php" class="item-news">
                                        <div class="box-img">
                                            <div class="thumbnail-img">
                                                <img src="assets/img/berita/berita3.jpg">
                                            </div>
                                        </div>
                                        <div class="box-post">

                                            <h3 class="post-title" style="text-transform: uppercase;">
                                                <span>Sapi bermutu</span>
                                            </h3>
                                            <div class="summary">
                                                <p><strong>DINAS &ndash;</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                            </div>
                                            <div class="post-date">
                                                November 02, 2021 </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xl-3">
                                    <a href="berita_detail.php" class="item-news">
                                        <div class="box-img">
                                            <div class="thumbnail-img">
                                                <img src="assets/img/berita/berita4.jpg">
                                            </div>
                                        </div>
                                        <div class="box-post">

                                            <h3 class="post-title" style="text-transform: uppercase;">
                                                <span>Kontes Sapi</span>
                                            </h3>
                                            <div class="summary">
                                                <p><strong>DINAS &ndash;</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                            </div>
                                            <div class="post-date">
                                                November 02, 2021 </div>
                                        </div>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <nav aria-label="Page navigation">
                            <!-- <ul class="pagination">
                                <li class="active"><a href="#!">1</a></li>
                                <li><a href="" data-ci-pagination-page="2">2</a></li>
                                <li><a href="" data-ci-pagination-page="3">3</a></li>
                                <li><a href="" data-ci-pagination-page="4">4</a></li>
                                <li class="next"><a href="" data-ci-pagination-page="2" rel="next"><i class="ti-control-forward"></i></a></li><a href="" data-ci-pagination-page="22"></a>
                            </ul> -->
                            <ul class="pagination">
                                <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </main>
        </main>

        <script>
            //STICKY-SIDEBAR
            $(document).ready(function() {
                $('.sidebar')
                    .theiaStickySidebar({
                        additionalMarginTop: 80,
                        minWidth: 1200
                    });
            });

            //BTN NAV-TRIGGER
            $('.btn-subnav').click(function() {
                $('.sidebar-nav').slideToggle('fast');
            });

            $('.list-videos').lightGallery({
                selector: '.play-button',
                youtubeThumbSize: 'maxresdefault',
                youtubePlayerParams: {
                    modestbranding: 1,
                    showinfo: 0,
                    rel: 0,
                    controls: 1
                }
            });
        </script>
        <?php include('general_footer.php'); ?>