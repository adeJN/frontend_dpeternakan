<?php include('general_header.php'); ?>

<body style="background: url(images/dotted.png); background-color: #e5e5e5;">
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<div id="page">
		<?php include('general_navbar.php'); ?>
		<main>
			<main>
				<div class="banner-breadcrumb">
					<div class="container">
						<div class="banner-content">
							<div class="banner-content-text">
								<div class="title-heading text-center">
									<h2>Komoditas Unggulan</h2>
								</div>
							</div>
							<nav class="breadcrumb">
								<ul class="breadcrumb-list">
									<li><a href="">Home</a></li>
									<li><a href="">Data Komoditas</a></li>
									<li class="current">Komoditas Unggulan</li>
								</ul>
							</nav>

						</div>
					</div>
					<div class="thumbnail-img">
						<img src="assets/img/berita/berita1.jpg">
					</div>
				</div>
				<div class="box-wrap">
					<div class="container">
						<div class="row">
							<div class="sidebar col-lg-3 col-md-3">
								<aside>
									<nav class="sidebar-nav">
										<ul>
											<li><a href="http://disbun.jabarprov.go.id/page/view/24-id-komoditas-strategis">Komoditas Strategis</a></li>
											<li><a href="http://disbun.jabarprov.go.id/page/view/25-id-komoditas-prospektif">Komoditas Prospektif</a></li>
											<li class="active"><a href="http://disbun.jabarprov.go.id/page/view/26-id-komoditas-unggulan">Komoditas Unggulan</a></li>
										</ul>
									</nav>
								</aside>
							</div>
							<div class="col-lg-9 col-md-9">
								<article>
									<p><strong>Komoditas Unggulan Spesifik Lokal,&nbsp;</strong>yaitu komoditas tertentu yang hanya ada di wilayah Kabupaten/Kota dan mempunyai potensi untuk menjadi komoditas andalan Kabupaten/Kota sesuai dengan keunggulannya.</p>
								</article>
								<h5 class="title-sub">Item Komoditas</h5>
								<div class="list-sub-komoditas">
									<div class="row justify-content-center">
										<div class="col-6 col-lg-3">
											<a href="http://disbun.jabarprov.go.id/page/view/64-id-pinang" class="item-sub-komoditas">
												<div class="box-img">
													<div class="thumbnail-img">
														<img src="http://disbun.jabarprov.go.id/cassets/libs/uploads/komoditas/pinang_merah_thumb.jpg">
													</div>
												</div>
												<h6 class="box-title">
													Pinang </h6>
											</a>
										</div>
										<div class="col-6 col-lg-3">
											<a href="http://disbun.jabarprov.go.id/page/view/65-id-kapok" class="item-sub-komoditas">
												<div class="box-img">
													<div class="thumbnail-img">
														<img src="http://disbun.jabarprov.go.id/cassets/libs/uploads/komoditas/Kapok_thumb.jpg">
													</div>
												</div>
												<h6 class="box-title">
													Kapok </h6>
											</a>
										</div>
										<div class="col-6 col-lg-3">
											<a href="http://disbun.jabarprov.go.id/page/view/66-id-akar-wangi" class="item-sub-komoditas">
												<div class="box-img">
													<div class="thumbnail-img">
														<img src="http://disbun.jabarprov.go.id/cassets/libs/uploads/komoditas/akar_wangi_thumb.jpg">
													</div>
												</div>
												<h6 class="box-title">
													Akar Wangi </h6>
											</a>
										</div>
										<div class="col-6 col-lg-3">
											<a href="http://disbun.jabarprov.go.id/page/view/67-id-sereh-wangi" class="item-sub-komoditas">
												<div class="box-img">
													<div class="thumbnail-img">
														<img src="http://disbun.jabarprov.go.id/cassets/libs/uploads/komoditas/sereh_wangi_thumb.jpg">
													</div>
												</div>
												<h6 class="box-title">
													Sereh Wangi </h6>
											</a>
										</div>
										<div class="col-6 col-lg-3">
											<a href="http://disbun.jabarprov.go.id/page/view/68-id-kina" class="item-sub-komoditas">
												<div class="box-img">
													<div class="thumbnail-img">
														<img src="http://disbun.jabarprov.go.id/cassets/libs/uploads/komoditas/kina_thumb.jpg">
													</div>
												</div>
												<h6 class="box-title">
													Kina </h6>
											</a>
										</div>
										<div class="col-6 col-lg-3">
											<a href="http://disbun.jabarprov.go.id/page/view/69-id-kenanga" class="item-sub-komoditas">
												<div class="box-img">
													<div class="thumbnail-img">
														<img src="http://disbun.jabarprov.go.id/cassets/libs/uploads/komoditas/bunga_kenanga_thumb.jpg">
													</div>
												</div>
												<h6 class="box-title">
													Kenanga </h6>
											</a>
										</div>
										<div class="col-6 col-lg-3">
											<a href="http://disbun.jabarprov.go.id/page/view/70-id-mendong" class="item-sub-komoditas">
												<div class="box-img">
													<div class="thumbnail-img">
														<img src="http://disbun.jabarprov.go.id/cassets/libs/uploads/komoditas/mendong_thumb.jpg">
													</div>
												</div>
												<h6 class="box-title">
													Mendong </h6>
											</a>
										</div>
										<div class="col-6 col-lg-3">
											<a href="http://disbun.jabarprov.go.id/page/view/71-id-pandan" class="item-sub-komoditas">
												<div class="box-img">
													<div class="thumbnail-img">
														<img src="http://disbun.jabarprov.go.id/cassets/libs/uploads/komoditas/Pandan_thumb.jpg">
													</div>
												</div>
												<h6 class="box-title">
													Pandan </h6>
											</a>
										</div>
										<div class="col-6 col-lg-3">
											<a href="http://disbun.jabarprov.go.id/page/view/72-id-guttapercha" class="item-sub-komoditas">
												<div class="box-img">
													<div class="thumbnail-img">
														<img src="http://disbun.jabarprov.go.id/cassets/libs/uploads/komoditas/gutta_thumb.jpg">
													</div>
												</div>
												<h6 class="box-title">
													Guttapercha </h6>
											</a>
										</div>
										<div class="col-6 col-lg-3">
											<a href="http://disbun.jabarprov.go.id/page/view/73-id-kumis-kucing" class="item-sub-komoditas">
												<div class="box-img">
													<div class="thumbnail-img">
														<img src="http://disbun.jabarprov.go.id/cassets/libs/uploads/komoditas/kumis_kucing_thumb.jpg">
													</div>
												</div>
												<h6 class="box-title">
													Kumis Kucing </h6>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
		</main>
		<script>
			//STICKY-SIDEBAR
			$(document).ready(function() {
				$('.sidebar')
					.theiaStickySidebar({
						additionalMarginTop: 80,
						minWidth: 1200
					});
			});

			//BTN NAV-TRIGGER
			$('.btn-subnav').click(function() {
				$('.sidebar-nav').slideToggle('fast');
			});

			$('.list-videos').lightGallery({
				selector: '.play-button',
				youtubeThumbSize: 'maxresdefault',
				youtubePlayerParams: {
					modestbranding: 1,
					showinfo: 0,
					rel: 0,
					controls: 1
				}
			});
		</script>
		<?php include('general_footer.php'); ?>