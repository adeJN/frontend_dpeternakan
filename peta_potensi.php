<?php include('general_header.php'); ?>

<body style="background: url(images/dotted.png); background-color: #e5e5e5;">
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<div id="page">
		<?php include('general_navbar.php'); ?>
		<main>
			<main>
				<div class="banner-breadcrumb">
					<div class="container">
						<div class="banner-content">
							<div class="banner-content-text">
								<div class="title-heading text-center">
									<h2>Komoditas Unggulan</h2>
								</div>
							</div>
							<nav class="breadcrumb">
								<ul class="breadcrumb-list">
									<li><a href="">Home</a></li>
									<li><a href="">Data Komoditas</a></li>
									<li class="current">Komoditas Unggulan</li>
								</ul>
							</nav>

						</div>
					</div>
					<div class="thumbnail-img">
						<img src="assets/img/berita/berita1.jpg">
					</div>
				</div>
				<div class="box-wrap">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<img src="assets/img/peta.png">
							</div>
						</div>
					</div>
				</div>
			</main>
		</main>
		<script>
			//STICKY-SIDEBAR
			$(document).ready(function() {
				$('.sidebar')
					.theiaStickySidebar({
						additionalMarginTop: 80,
						minWidth: 1200
					});
			});

			//BTN NAV-TRIGGER
			$('.btn-subnav').click(function() {
				$('.sidebar-nav').slideToggle('fast');
			});

			$('.list-videos').lightGallery({
				selector: '.play-button',
				youtubeThumbSize: 'maxresdefault',
				youtubePlayerParams: {
					modestbranding: 1,
					showinfo: 0,
					rel: 0,
					controls: 1
				}
			});
		</script>
		<?php include('general_footer.php'); ?>