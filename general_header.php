<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dinas Peternakan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/img/logo-icon.png" />

    <link rel="canonical" href="" />

    <meta name="description" content="Dinas" />
    <meta name="keywords" content="" />

    <meta name="google-site-verification" content="" />

    <meta name="robots" content="index,follow" />
    <meta name="googlebot" content="index,follow" />
    <meta name="revisit-after" content="2 days" />
    <meta name="author" content="4 Vision Media">
    <meta name="expires" content="never" />

    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="" name="title" />
    <meta property="og:description" name="description" content="" />
    <meta property="og:url" name="url" content="" />
    <meta property="og:site_name" content="" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:title" content="" />

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Frank+Ruhl+Libre:400,700|Roboto+Condensed:400,700" rel="stylesheet">

    <!-- Css Global -->
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/fontawesome-all.css">

    <!-- jQuery Additional-->
    <script src="assets/js/theia-sticky-sidebar.js"></script>

    <!-- Css Additional -->
    <link rel="stylesheet" href="assets/css/lightgallery.css">
    <link rel="stylesheet" href="assets/css/swiper.min.css">

    <script src="assets/js/jquery.min.js"></script>

    <!-- jQuery Additional-->
    <script src="assets/js/swiper.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>

    <script src="assets/js/masonry.pkgd.min.js"></script>
    <script src="assets/js/lightgallery.js"></script>
    <script src="assets/js/lg/lg-video.js"></script>
    <script src="assets/js/lg/lg-zoom.js"></script>
    <script src="assets/js/lg/lg-thumbnail.js"></script>

    <!-- jQuery Global-->
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/jquery.video.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/fill.box.js"></script>
    <script src="assets/js/main.js"></script>
</head>