<?php include('general_header.php'); ?>

<body style=" background-color: #e5e5e5;">
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c5d8b22ddc43907"></script> -->
    <div id="page">

        <?php include('general_navbar.php'); ?>
        <main>
            <main>
                <div class="banner-breadcrumb">
                    <div class="container">
                        <div class="banner-content">
                            <div class="banner-content-text">
                                <div class="title-heading text-center">
                                    <h2>Kontak Kami</h2>
                                </div>
                            </div>
                            <nav class="breadcrumb">
                                <ul class="breadcrumb-list">
                                    <li>
                                        <a href="">
                                            Home
                                        </a>
                                    </li>
                                    <li class="current">
                                        Kontak Kami </li>
                                </ul>
                            </nav>

                        </div>
                    </div>
                    <div class="thumbnail-img">
                        <img src="assets/img/gallery.jpg">
                    </div>
                </div>
                <div class="box-wrap contact-content">
                    <div class="container">
                        <div class="contact-flex">
                            <div class="contact-info">
                                <div class="contact-desc" style="margin-top: 170px;">
                                    <h4>DINAS </h4>
                                    <p></p>
                                    <p>Jalanan</p>
                                    <p></p>
                                    <p>Telp: (0341) 123435 </p>
                                    <p>Email 1: email@email.co.id</p>
                                    <!-- <p>SMS Center: -</p> -->

                                </div>
                                <div class="contact-inquiry">
                                    <div class="title-heading text-left">
                                        <h1>Form Contact</h1>
                                    </div>
                                    <form action="" novalidate="" class="needs-validation" method="post" accept-charset="utf-8">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="name" class="important">Name</label>
                                                    <input id="name" class="form-control" name="name" value="" required="" type="text">

                                                    <div class="invalid-feedback">Nama wajib untuk diisi</div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="email" class="important">Email</label>
                                                    <input id="email" class="form-control" name="email" value="" required="" type="email">
                                                    <div class="invalid-feedback">Email wajib untuk diisi</div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="company" class="important">Pesan</label>
                                                    <textarea id="company" class="form-control" name="content" value="" type="text" required=""></textarea>
                                                    <div class="invalid-feedback">Pesan wajib untuk diisi</div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-right">
                                                <button class="btn btn-primary" type="submit">Send Message</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <script>
                //VALIDATION FORM
                (function() {
                    'use strict';
                    window.addEventListener('load', function() {
                        // Fetch all the forms we want to apply custom Bootstrap validation styles to
                        var forms = document.getElementsByClassName('needs-validation');
                        // Loop over them and prevent submission
                        var validation = Array.prototype.filter.call(forms, function(form) {
                            form.addEventListener('submit', function(event) {
                                if (form.checkValidity() === false) {
                                    event.preventDefault();
                                    event.stopPropagation();
                                }
                                form.classList.add('was-validated');
                            }, false);
                        });
                    }, false);
                })();
            </script>
        </main>
        <script>
            //STICKY-SIDEBAR
            $(document).ready(function() {
                $('.sidebar')
                    .theiaStickySidebar({
                        additionalMarginTop: 80,
                        minWidth: 1200
                    });
            });

            //BTN NAV-TRIGGER
            $('.btn-subnav').click(function() {
                $('.sidebar-nav').slideToggle('fast');
            });

            $('.list-videos').lightGallery({
                selector: '.play-button',
                youtubeThumbSize: 'maxresdefault',
                youtubePlayerParams: {
                    modestbranding: 1,
                    showinfo: 0,
                    rel: 0,
                    controls: 1
                }
            });
        </script>
        <?php include('general_footer.php'); ?>