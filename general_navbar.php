<header>
    <div class="main-header">
        <div class="container-fluid">
            <div class="menubar-flex">
                <div class="menubar-left">
                    <div class="main-logo">
                        <a href="">
                            <img src="assets/img/logo.png">
                        </a>
                    </div>
                </div>
                <div class="menubar-center">
                    <nav class="main-nav">
                        <ul class="list-nav">
                            <li class="current-nav"><a href="index.php">Beranda</a>
                            </li>
                            <li class="has-dropdown"><a href="#">Tentang Kami</a>
                                <ul class="dropdown">
                                    <li class="has-dropdown"><a href="berita.php">Sambutan Kadisnak</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Sejarah</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Profil & Struktur Organisasi</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Rencana Strategis</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Sekretariat</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Bidang Pembibitan, Pakan dan Produksi Peternakan</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Bidang Kesehatan Hewan</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Bidang PPHP </a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Bidang Kesehatan Masyarakat Veteriner</a>
                                    </li>
                                    <li class="has-dropdown"><a href="kontak.php">Kontak Kami</a>
                                    </li>

                                </ul>
                            </li>
                            <li class="has-dropdown"><a href="#">UPT & LAB</a>
                                <ul class="dropdown">
                                    <li class="has-dropdown"><a href="berita.php">Berita</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Berita</a>
                                    </li>

                                </ul>
                            </li>
                            <li class="has-dropdown"><a href="#">Alur Perizinan</a>
                                <ul class="dropdown">
                                    <li class="has-dropdown"><a href="berita.php">Alur pelayanan laboratorium kesmavet</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Alur pelayanan laboratorium pakan</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Alur pemeriksaan kesehatan di RSH</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Alur rekomendasi Sertifikat NKV</a>
                                    </li>

                                </ul>
                            </li>
                            <li class="has-dropdown"><a href="#">Informasi</a>
                                <ul class="dropdown">
                                    <li class="has-dropdown"><a href="berita.php">Berita</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Info Kegiatan</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Komoditas Unggulan</a>
                                    </li>
                                    <li class="has-dropdown"><a href="peta_potensi.php">Peta Potensi</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Alamat Dinas</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Usaha Peternakan</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="has-dropdown"><a href="#">Layanan Publik</a>
                                <ul class="dropdown">
                                    <li class="has-dropdown"><a href="berita.php">Info Harga</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Data Statistik</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Forum Bisnis</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Forum Konsultasi</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Klinik Online</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Produk Hukum/ Kebijakan</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Artikel Dan Jurnal</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Opini Anda</a>
                                    </li>
                                    <li class="has-dropdown"><a href="berita.php">Sipasnak</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- <li class="has-dropdown"><a href="#">Tentang Kami</a>
                                <ul class="dropdown">
                                    <li class="has-dropdown"><a href="berita.php">Berita</a>
                                    </li>
                                    <li class="has-dropdown"><a href="#">Bidang</a>
                                        <ul class="sub-dropdown">
                                            <li class="has-dropdown"><a href="">Bidang Produksi </a></li>
                                            <li class="has-dropdown"><a href="">Bidang Sumberdaya</a></li>
                                            <li class="has-dropdown"><a href="">Bidang Pengembangan dan Perlindungan </a></li>
                                            <li class="has-dropdown"><a href="">Bidang Pengolahan Pemasaran Dan Usaha </a></li>
                                        </ul>
                                    </li>

                                </ul>
                            </li> -->
                            <li class="has-dropdown"><a href="#">Inovasi</a>
                                <ul class="dropdown">
                                    <li class="has-dropdown"><a href="gallery_foto.php">Intan Salaksa Care</a>
                                    </li>
                                    <li class="has-dropdown"><a href="gallery_video.php">Inovasi 3 PIPO</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="has-dropdown"><a href="#">Galeri</a>
                                <ul class="dropdown">
                                    <li class="has-dropdown"><a href="gallery_foto.php">Foto</a>
                                    </li>
                                    <li class="has-dropdown"><a href="gallery_video.php">Video</a>
                                    </li>
                                    <li class="has-dropdown"><a href="gallery_video.php">Pustaka Digital</a>
                                    </li>
                                    <li class="has-dropdown"><a href="gallery_video.php">Download Arsip</a>
                                    </li>
                                    <li class="has-dropdown"><a href="gallery_video.php">Leaflet</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="current-nav"><a href="https://ppid.disnak.jatimprov.go.id/">PPID</a>
                            </li>
                        </ul>
                        <div class="btn-close">
                            <i class="ti-close"></i>
                        </div>
                    </nav>
                    <div class="search-nav">
                        <span class="search-btn" data-selector=".search-nav"></span>
                    </div>
                    <div class="navigation-burger">
                        <span></span>
                    </div>
                </div>
                <div class="menubar-right">
                    <div class="garuda-logo">
                        <img src="assets/img/garuda.svg">
                    </div>
                    <!-- <ul class="language-bar">
                        <li class="active"><a href="lang/id">ID</a></li>
                        <li class=""><a href="lang/en">EN</a></li>
                    </ul> -->
                </div>

            </div>
        </div>
    </div>
    <div class="search-wrap">
        <div class="btn-close">
            <i class="ti-close"></i>
        </div>
        <form action="">
            <div class="form-group">
                <input id="search-header" type="search" name="q" class="form-control" placeholder="Type here to search">
                <button class="btn btn-icon" type="submit"><i class="ti ti-search"></i></button>
            </div>
        </form>
    </div>
</header>