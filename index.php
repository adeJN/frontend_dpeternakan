<?php include('general_header.php'); ?>

<body>
    <div id="page">
        <?php include('general_navbar.php'); ?>
        <main>
            <div class="home-intro">
                <div class="container">
                    <div class="intro-flex">
                        <div class="intro-content">
                            <div class="title-heading">
                                <h4>Selamat Datang Di Website</h4>
                                <h2>Dinas</h2>
                            </div>
                            <div class="intro-btn">
                                <span class="video-source" data-src="https://www.youtube.com/watch?v=3VgZcy7qVPM">
                                    <i class="ti-control-play"></i>
                                </span>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="thumb" data-vide-bg="mp4: assets/video/video.mp4, poster: assets/img/img-bg.jpg" data-vide-options="posterType:jpg"></div>
            </div>
            <div class="box-wrap bg-grey-1 news">
                <div class="container">
                    <div class="title-heading text-center">
                        <h6>DINAS</h6>
                        <h2>Berita Terkini</h2>
                    </div>
                    <div class="list-news">
                        <div class="row ">
                            <div class="swiper-container swiper-2">
                                <div class=" swiper-wrapper">

                                    <div class="col-md-6 col-lg-6 col-xl-3 swiper-slide">
                                        <a href="berita_detail.php" class="item-news">
                                            <div class="box-img">
                                                <div class="thumbnail-img">
                                                    <img src="assets/img/berita/berita1.jpg">
                                                </div>
                                            </div>
                                            <div class="box-post">
                                                <h3 class="post-title">
                                                    <span>BAZAR TERNAK QURBAN 2019</span>
                                                </h3>
                                                <div class="summary">
                                                    <p><strong>Malang &ndash;</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum..</p>
                                                </div>
                                                <div class="post-date">
                                                    November 02, 2021 </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-3 swiper-slide">
                                        <a href="berita_detail.php" class="item-news">
                                            <div class="box-img">
                                                <div class="thumbnail-img">
                                                    <img src="assets/img/berita/berita2.jpg">
                                                </div>
                                            </div>
                                            <div class="box-post">
                                                <h3 class="post-title">
                                                    <span>Kontes ayam 2020</span>
                                                </h3>
                                                <div class="summary">
                                                    <p><strong>Malang &ndash;</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum..</p>
                                                </div>
                                                <div class="post-date">
                                                    November 02, 2021 </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-3 swiper-slide">
                                        <a href="berita_detail.php" class="item-news">
                                            <div class="box-img">
                                                <div class="thumbnail-img">
                                                    <img src="assets/img/berita/berita3.jpg">
                                                </div>
                                            </div>
                                            <div class="box-post">
                                                <h3 class="post-title">
                                                    <span>Budidaya sapi </span>
                                                </h3>
                                                <div class="summary">
                                                    <p><strong>Malang &ndash;</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum..</p>
                                                </div>
                                                <div class="post-date">
                                                    November 02, 2021 </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-3 swiper-slide">
                                        <a href="berita_detail.php" class="item-news">
                                            <div class="box-img">
                                                <div class="thumbnail-img">
                                                    <img src="assets/img/berita/berita4.jpg">
                                                </div>
                                            </div>
                                            <div class="box-post">
                                                <h3 class="post-title">
                                                    <span>Kontes Sapi terbaik</span>
                                                </h3>
                                                <div class="summary">
                                                    <p><strong>Malang &ndash;</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum..</p>
                                                </div>
                                                <div class="post-date">
                                                    November 02, 2021 </div>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                    </div>
                    <div class="box-btn text-center">
                        <a href="berita.php" class="btn btn-primary">
                            Berita Lainnya </a>
                    </div>
                </div>
            </div>

            <div class="box-wrap gallery-home row">
                <div class="title-heading text-white text-center">
                    <h6>DINAS</h6>
                    <h2>Foto dan Video Kegiatan</h2>
                </div>
                <div class="album-photos home col-lg-6">
                    <div class="container">
                        <!-- <div class="title-heading text-white text-center">
                            <h6>DINAS</h6>
                            <h2>Foto dan Video Kegiatan</h2>
                        </div> -->
                        <div class="row justify-content-center">

                            <div class="col-lg-6 col-md-12 col-sm-6">
                                <figure class="item-album">
                                    <div class="thumb-icon">
                                        <i class="ti-gallery"></i>
                                    </div>
                                    <figcaption class="album-content">
                                        <span class="title-album">Kegiatan 2021</span>
                                        <span class="amount-album">0 Photos</span>
                                        <a href="gallery_foto.php" class="btn btn-primary">Selengkapnya</a>
                                    </figcaption>
                                    <figure class="thumbnail-img">
                                        <img src="assets/img/berita/berita1.jpg">
                                    </figure>
                                </figure>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <figure class="item-album">
                                    <div class="thumb-icon">
                                        <i class="ti-gallery"></i>
                                    </div>
                                    <figcaption class="album-content">
                                        <span class="title-album">Kegiatan 2021</span>
                                        <span class="amount-album">0 Photos</span>
                                        <a href="gallery_foto.php" class="btn btn-primary">Selengkapnya</a>
                                    </figcaption>
                                    <figure class="thumbnail-img">
                                        <img src="assets/img/gallery.jpg">
                                    </figure>
                                </figure>
                            </div>

                        </div>
                        <div class="box-btn text-center">
                            <a href="gallery_foto.php" class="btn btn-primary">
                                Foto Lainnya </a>
                        </div>
                    </div>
                </div>
                <div class="list-videos col-lg-6">
                    <div class="container">
                        <!-- <div class="title-heading text-white text-center">
                            <h6>DINAS</h6>
                            <h2>Video Kegiatan</h2>
                        </div> -->
                        <div class="row justify-content-center">


                            <div class="col-lg-6 col-md-12 col-sm-6">
                                <figure class="item-video">
                                    <div class="thumb-icon">
                                        <i class="ti-control-play"></i>
                                    </div>
                                    <figcaption class="video-content">
                                        <span class="title-video">Video Kegiatan 2021</span>
                                        <span class="play-button" data-src="https://www.youtube.com/watch?v=JbVKx34UJOY" data-poster="https://i.ytimg.com/vi/JbVKx34UJOY/hqdefault.jpg">
                                            <i class="ti-control-play"></i>
                                        </span>
                                    </figcaption>
                                    <figure class="thumbnail-img">
                                        <img src="assets/img/berita/berita3.jpg">
                                    </figure>
                                </figure>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <figure class="item-video">
                                    <div class="thumb-icon">
                                        <i class="ti-control-play"></i>
                                    </div>
                                    <figcaption class="video-content">
                                        <span class="title-video">Video Kegiatan 2021</span>
                                        <span class="play-button" data-src="https://www.youtube.com/watch?v=JbVKx34UJOY" data-poster="https://i.ytimg.com/vi/JbVKx34UJOY/hqdefault.jpg">
                                            <i class="ti-control-play"></i>
                                        </span>
                                    </figcaption>
                                    <figure class="thumbnail-img">
                                        <img src="assets/img/berita/berita4.jpg">
                                    </figure>
                                </figure>
                            </div>

                        </div>
                        <div class="box-btn text-center">
                            <a href="gallery_video.php" class="btn btn-primary">
                                Video Lainnya </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-counter" style="background: url(assets/img/bg-counter.jpg);">
                <div class="data-counter">
                    <div class="container">
                        <div class="title-heading text-center">
                            <h6>DINAS</h6>
                            <h2>Komoditas Peternakan Jawa Timur</h2>
                        </div>
                        <div class="row justify-content-md-center">
                            <div class="col-md-6 col-lg-4">
                                <div class="widget-komoditas">
                                    <h5>Informasi Kegiatan</h5>
                                    <div class="widget-content">
                                        <ul class="list-widget-komoditas">
                                            <li>
                                                <a href="http://disbun.jabarprov.go.id/page/view/24-id-komoditas-strategis">Komoditas Strategis</a>
                                            </li>
                                            <li>
                                                <a href="komoditas_unggulan.php">Komoditas Unggul Lokal</a>
                                            </li>
                                        </ul>
                                        <div class="thumbnail-img">
                                            <img src="http://disbun.jabarprov.go.id/tmplts/disbun/assets/images/bg-komoditas.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <div class="widget-komoditas">
                                    <h5>Forum Konsultasi</h5>
                                    <div class="widget-content">
                                        <a href="http://disbun.jabarprov.go.id/GIS_DISBUN_2019" target="_blank">
                                            <div class="thumbnail-img">
                                                <img src="http://disbun.jabarprov.go.id/tmplts/disbun/assets/images/gis-2019.jpg" alt="">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <div class="widget-komoditas">
                                    <h5>Klinik Online</h5>
                                    <div class="swiper-container swiper-5">
                                        <div class="swiper-wrapper info-graphic">
                                            <div class="swiper-slide" data-src="http://disbun.jabarprov.go.id/cassets/libs/uploads/1.png">
                                                <div class="thumbnail-img">
                                                    <img src="http://disbun.jabarprov.go.id/cassets/libs/uploads/1.png" alt="">
                                                </div>
                                            </div>
                                            <div class="swiper-slide" data-src="http://disbun.jabarprov.go.id/cassets/libs/uploads/2.png">
                                                <div class="thumbnail-img">
                                                    <img src="http://disbun.jabarprov.go.id/cassets/libs/uploads/2.png" alt="">
                                                </div>
                                            </div>
                                            <div class="swiper-slide" data-src="http://disbun.jabarprov.go.id/cassets/libs/uploads/3.png">
                                                <div class="thumbnail-img">
                                                    <img src="http://disbun.jabarprov.go.id/cassets/libs/uploads/3.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-pagination"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="box-wrap partners">
                <div class="container">
                    <div class="title-heading text-center">
                    </div>
                    <h6 class="title-sub text-center">Partner</h6>
                    <div class="list-partners">
                        <a href="" target="_blank" class="item-partner">
                            <img src="assets/img/kementanpertanian.svg">
                        </a>
                        <a href="" target="_blank" class="item-partner">
                            <img src="assets/img/logo.png">
                        </a>
                        <a href="" target=" _blank" class="item-partner">
                            <img src="assets/img/jatim.png">
                        </a>
                        <a href="" target="_blank" class="item-partner">
                            <img src="assets/img/logo-kominfo.png">
                        </a>
                        <a href="" target="_blank" class="item-partner">
                            <img src="assets/img/lpse.svg">
                        </a>
                    </div>
                </div>
            </div>
        </main>

        <?php include('general_footer.php'); ?>