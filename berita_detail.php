<?php include('general_header.php'); ?>

<body style="background: url(images/dotted.png); background-color: #e5e5e5;">
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c5d8b22ddc43907"></script>
    <div id="page">
        <?php include('general_navbar.php'); ?>
        <main>
            <main>
                <div class="banner-breadcrumb">
                    <div class="container">
                        <div class="banner-content">
                            <div class="banner-content-text">
                                <div class="title-heading text-center">
                                    <h2>Berita Tentang Hari ini 2021</h2>
                                </div>
                            </div>
                            <nav class="breadcrumb">
                                <ul class="breadcrumb-list">
                                    <li><a href="">Home</a></li>
                                    <li><a href="berita.php">Berita</a></li>
                                    <li class="current">Berita tentang...</li>
                                </ul>
                            </nav>

                        </div>
                    </div>
                    <div class="thumbnail-img">
                        <img src="assets/img/gallery.jpg">
                    </div>
                </div>
                <!-- isi detail berita -->

                <div class="box-wrap">
                    <div class="row">
                        <div class="container content-detail col-md-7" style="margin-left:100px;">
                            <div class="box-img">
                                <img src="assets/img/berita/berita2.jpg">
                            </div>
                            <div class="box-post">
                                <div class="post-date">
                                    11 November, 2021 </div>
                            </div>
                            <div class="box-content">
                                <article>
                                    <p><strong>DINAS &ndash;</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    <p>&nbsp;</p>
                                    <p>Editor:&nbsp;<a>Lorem</a></p>
                                    <p>Jurnalis: Ipsum</p>
                                    <p>Sumber:&nbsp;www.</p>
                                </article>
                            </div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div><br>
                            <h5><strong>Dibaca :</strong>
                                <font color="#009641"><strong>91 kali</strong></font>
                            </h5>
                            <div class="fb-comments" data-href="" data-width="100%" data-numposts="5">
                            </div>
                        </div>
                        <div class="container col-md-4">
                            <div class="box-content">
                                <b>INFO BERITA</b>
                                <hr>
                                <div class="media" style="margin-bottom: 10px;">
                                    <div class="pull-left">
                                        <img src="http://disnak.jatimprov.go.id/web/upload_data/images/2021/11/thumb/thumb_20211104072727_whatsappimage20211104at11.35.30.jpeg" width="70" height="70" alt="alur pengujian lab kesmavet" style="border-radius: 50%;margin-right: 10px;" />
                                    </div>
                                    <div class="media-body">
                                        <small><strong>4-November-2021</strong></small>
                                        <h6 class="media-heading"><a href="#" title="Alur Pelayanan Pengujian Lab Kesmavet Disnak Jatim">Alur Pelayanan Pengujian Lab Kesmavet Disnak Jatim</a></h6>
                                    </div>
                                </div>
                                <div class="media" style="margin-bottom: 10px;">
                                    <div class="pull-left">
                                        <img src="http://disnak.jatimprov.go.id/web/upload_data/images/2021/11/thumb/thumb_20211104072727_whatsappimage20211104at11.35.30.jpeg" width="70" height="70" alt="alur pengujian lab kesmavet" style="border-radius: 50%;margin-right: 10px;" />
                                    </div>
                                    <div class="media-body">
                                        <small><strong>4-November-2021</strong></small>
                                        <h6 class="media-heading"><a href="#" title="Alur Pelayanan Pengujian Lab Kesmavet Disnak Jatim">Alur Pelayanan Pengujian Lab Kesmavet Disnak Jatim</a></h6>
                                    </div>
                                </div>
                                <div class="media" style="margin-bottom: 10px;">
                                    <div class="pull-left">
                                        <img src="http://disnak.jatimprov.go.id/web/upload_data/images/2021/11/thumb/thumb_20211104072727_whatsappimage20211104at11.35.30.jpeg" width="70" height="70" alt="alur pengujian lab kesmavet" style="border-radius: 50%;margin-right: 10px;" />
                                    </div>
                                    <div class="media-body">
                                        <small><strong>4-November-2021</strong></small>
                                        <h6 class="media-heading"><a href="#" title="Alur Pelayanan Pengujian Lab Kesmavet Disnak Jatim">Alur Pelayanan Pengujian Lab Kesmavet Disnak Jatim</a></h6>
                                    </div>
                                </div>
                            </div>
                            <div class="box-content" style="margin-top: 20px;">
                                <b>
                                    INFO KEGIATAN
                                </b>
                                <hr>

                                <div class="media" style="margin-bottom: 10px;">
                                    <div class="pull-left">
                                        <img src="http://disnak.jatimprov.go.id/web/upload_data/images/2021/11/thumb/thumb_20211104072727_whatsappimage20211104at11.35.30.jpeg" width="70" height="70" alt="alur pengujian lab kesmavet" style="border-radius: 50%;margin-right: 10px;" />
                                    </div>
                                    <div class="media-body">
                                        <small><strong>4-November-2021</strong></small>
                                        <h6 class="media-heading"><a href="#" title="Alur Pelayanan Pengujian Lab Kesmavet Disnak Jatim">Alur Pelayanan Pengujian Lab Kesmavet Disnak Jatim</a></h6>
                                    </div>
                                </div>
                                <div class="media" style="margin-bottom: 10px;">
                                    <div class="pull-left">
                                        <img src="http://disnak.jatimprov.go.id/web/upload_data/images/2021/11/thumb/thumb_20211104072727_whatsappimage20211104at11.35.30.jpeg" width="70" height="70" alt="alur pengujian lab kesmavet" style="border-radius: 50%;margin-right: 10px;" />
                                    </div>
                                    <div class="media-body">
                                        <small><strong>4-November-2021</strong></small>
                                        <h6 class="media-heading"><a href="#" title="Alur Pelayanan Pengujian Lab Kesmavet Disnak Jatim">Alur Pelayanan Pengujian Lab Kesmavet Disnak Jatim</a></h6>
                                    </div>
                                </div>
                                <div class="media" style="margin-bottom: 10px;">
                                    <div class="pull-left">
                                        <img src="http://disnak.jatimprov.go.id/web/upload_data/images/2021/11/thumb/thumb_20211104072727_whatsappimage20211104at11.35.30.jpeg" width="70" height="70" alt="alur pengujian lab kesmavet" style="border-radius: 50%;margin-right: 10px;" />
                                    </div>
                                    <div class="media-body">
                                        <small><strong>4-November-2021</strong></small>
                                        <h6 class="media-heading"><a href="#" title="Alur Pelayanan Pengujian Lab Kesmavet Disnak Jatim">Alur Pelayanan Pengujian Lab Kesmavet Disnak Jatim</a></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="recent-news sr">
                            <div class="title-heading">
                                <h4>Berita / Artikel Lainnya :</h4>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <a href="" class="prev-news">
                                        <div class="news-img">
                                            <div class="thumbnail-img">
                                                <img src="assets/img/gallery.jpg">
                                            </div>
                                        </div>
                                        <div class="news-info">
                                            <span>Sebelumnya</span>
                                            <h1 class="news-title">Berita Kemarin</h1>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <a href="" class="next-news">
                                        <div class="news-img">
                                            <div class="thumbnail-img">
                                                <img src="assets/img/gallery.jpg" />
                                            </div>
                                        </div>
                                        <div class="news-info">
                                            <span>Selanjutnya</span>
                                            <h1 class="news-title">Berita lain</h1>
                                        </div>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </main>
        </main>

        <script>
            //STICKY-SIDEBAR
            $(document).ready(function() {
                $('.sidebar')
                    .theiaStickySidebar({
                        additionalMarginTop: 80,
                        minWidth: 1200
                    });
            });

            //BTN NAV-TRIGGER
            $('.btn-subnav').click(function() {
                $('.sidebar-nav').slideToggle('fast');
            });

            $('.list-videos').lightGallery({
                selector: '.play-button',
                youtubeThumbSize: 'maxresdefault',
                youtubePlayerParams: {
                    modestbranding: 1,
                    showinfo: 0,
                    rel: 0,
                    controls: 1
                }
            });
        </script>
        <?php include('general_footer.php'); ?>