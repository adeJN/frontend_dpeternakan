<footer>
    <div class="footer">
        <div class="container-fluid">
            <div class="footer-flex">
                <div class="widget col-3">
                    <div class="f-title">
                        <h4>Sekilas Disnak</h4>
                    </div>
                    Sambutan Kadisnak
                    <br>
Struktur Organisasi
<br>
Kontak Kami
<br>
Peta Situs
                </div>

                <div class="widget col-3">
                    <div class="f-title">
                        <h4>Pranala Web</h4>
                        Kementrian Pertanian
                        <br>
Provinsi Jawa Timur
                        <br>
Ditjen Peternakan & Kesehatan Hewan
                        <br>
Ditjen Prasarana & Sarana Pertanian
                    </div>
                </div>
                <div class="widget col-3">
                    <div class="f-title">
                        <h4>Link Terkait</h4>
                    </div>
                    Simponi Ternak
                        <br>
Perizinan Terpadu
                        <br>
Klinik Online Disnak
                        <br>
Forum Konsultasi Peternakan
                </div>
                <div class="widget col-3">
                    <div class="f-title">
                        <h4>Sosial Media</h4>
                            <img src="assets/img/sosmed/fb.png" width="50px">
                            <img src="assets/img/sosmed/twitter.png" width="50px">
                            <img src="assets/img/sosmed/email.png" width="50px">

                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="widget">
                    © 2021 Dinas. Developed by <a href="" target="_blank" class="logo-4vm">colabs</a>
                </div>
            </div>
        </div>
    </div>
</footer> <a id="button"></a>
<!-- <div class="intro-social">
    <ul>
        <li class="instagram"><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
        <li class="twitter"><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
        <li class="facebook"><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
        <li class="youtube"><a href="#" target="_blank"><i class="fab fa-youtube"></i></a></li>
    </ul>
</div> -->
</div>
<script>
    //LIGHT GALLERY
    $('.intro-btn').lightGallery({
        selector: '.video-source',
        autoplay: true,
        videoMaxWidth: '80%',
        youtubePlayerParams: {
            modestbranding: 1,
            showinfo: 0,
            rel: 0,
            controls: 1
        }
    });

    $('.list-videos').lightGallery({
        selector: '.play-button',
        youtubeThumbSize: 'maxresdefault',
        youtubePlayerParams: {
            modestbranding: 1,
            showinfo: 0,
            rel: 0,
            controls: 1
        }
    });

    //SCROLL REVEAL
    window.sr = ScrollReveal({
        scale: 1,
        duration: 300,
        distance: 0,
        easing: 'cubic-bezier(0.42, 0, 0.58, 1)',
    });

    sr.reveal('.sr-repeat', {
        distance: '3em',
        origin: 'bottom',
        duration: 500
    }, 500);

    //MASONRY GALLERY
    $('#masonry').masonry({
        // options
        itemSelector: '.gallery-item'
    });

    $('.list-gallery').lightGallery({});

    $('.info-graphic').lightGallery({});

    //COUNTER
    jQuery(document).ready(function($) {
        $('.counter').counterUp({
            delay: 10,
            time: 1500
        });
    });

    //SLIDE
    var swiper1 = new Swiper('.swiper-1', {
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 0,
        autoHeight: true,
        nextButton: '.sbn',
        prevButton: '.sbp',
        loop: true,
        autoplay: true,
        autoplay: 7500,
        speed: 1000,
        pagination: '.swiper-pagination'
    });

    var swiper1 = new Swiper('.swiper-5', {
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 0,
        loop: true,
        autoplay: true,
        autoplay: 7500,
        speed: 1000,
        pagination: '.swiper-pagination'
    });


    //SLIDE-DESTROY
    const enableSwiper = function() {
        mySwiper = new Swiper('.swiper-container.swiper-2, .swiper-container.swiper-3, .swiper-container.swiper-4', {
            loop: true,
            // pagination
            pagination: '.swiper-pagination',
            paginationClickable: true,
        });
    };
</script>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.3"></script>
<script src="assets/js/swiper-destroy.js"></script>
</body>

</html>