<?php include('general_header.php'); ?>

<body style=" background-color: #e5e5e5;">
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c5d8b22ddc43907"></script> -->
    <div id="page">
        <?php include('general_navbar.php'); ?>
        <main>
            <main>
                <div class="banner-breadcrumb">
                    <div class="container">
                        <div class="banner-content">
                            <div class="banner-content-text">
                                <div class="title-heading text-center">
                                    <h2>Foto Kegiatan</h2>
                                </div>
                            </div>
                            <nav class="breadcrumb">
                                <ul class="breadcrumb-list">
                                    <li><a href="">Beranda</a></li>
                                    <li class="current">
                                        Foto </li>
                                </ul>
                            </nav>

                        </div>
                    </div>
                    <div class="thumbnail-img">
                        <img src="assets/img/gallery.jpg">
                    </div>
                </div>
                <div class="box-wrap">
                    <div class="album-photos">
                        <div class="container-fluid">
                            <div class="row justify-content-center">

                                <div class="col-lg-3 col-md-6 col-sm-6">
                                    <figure class="item-album">
                                        <div class="thumb-icon">
                                            <i class="ti-gallery"></i>
                                        </div>
                                        <figcaption class="album-content">
                                            <span class="title-album">Forum Perangkat Daerah 2021</span>
                                            <span class="amount-album">0 Photos</span>
                                            <a href="berita_detail.php" class="btn btn-primary">Selengkapnya</a>
                                        </figcaption>
                                        <figure class="thumbnail-img">
                                            <img src="assets/img/berita/berita1.jpg">
                                        </figure>
                                    </figure>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                    <figure class="item-album">
                                        <div class="thumb-icon">
                                            <i class="ti-gallery"></i>
                                        </div>
                                        <figcaption class="album-content">
                                            <span class="title-album">Forum Perangkat Daerah 2021</span>
                                            <span class="amount-album">0 Photos</span>
                                            <a href="berita_detail.php" class="btn btn-primary">Selengkapnya</a>
                                        </figcaption>
                                        <figure class="thumbnail-img">
                                            <img src="assets/img/berita/berita2.jpg">
                                        </figure>
                                    </figure>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                    <figure class="item-album">
                                        <div class="thumb-icon">
                                            <i class="ti-gallery"></i>
                                        </div>
                                        <figcaption class="album-content">
                                            <span class="title-album">Forum Perangkat Daerah 2021</span>
                                            <span class="amount-album">0 Photos</span>
                                            <a href="berita_detail.php" class="btn btn-primary">Selengkapnya</a>
                                        </figcaption>
                                        <figure class="thumbnail-img">
                                            <img src="assets/img/berita/berita3.jpg">
                                        </figure>
                                    </figure>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                    <figure class="item-album">
                                        <div class="thumb-icon">
                                            <i class="ti-gallery"></i>
                                        </div>
                                        <figcaption class="album-content">
                                            <span class="title-album">Forum Perangkat Daerah 2021</span>
                                            <span class="amount-album">0 Photos</span>
                                            <a href="berita_detail.php" class="btn btn-primary">Selengkapnya</a>
                                        </figcaption>
                                        <figure class="thumbnail-img">
                                            <img src="assets/img/berita/berita4.jpg">
                                        </figure>
                                    </figure>
                                </div>

                            </div>
                            <nav aria-label="Page navigation">
                                <!-- <ul class="pagination">
                                    <li class="active"><a href="#!">1</a></li>
                                    <li><a href="" data-ci-pagination-page="2">2</a></li>
                                    <li><a href="" data-ci-pagination-page="3">3</a></li>
                                    <li><a href="" data-ci-pagination-page="4">4</a></li>
                                    <li class="next"><a href="" data-ci-pagination-page="2" rel="next"><i class="ti-control-forward"></i></a></li><a href="" data-ci-pagination-page="10"></a>
                                </ul> -->
                                <ul class="pagination">
                                    <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </main>
        </main>
        <script>
            //STICKY-SIDEBAR
            $(document).ready(function() {
                $('.sidebar')
                    .theiaStickySidebar({
                        additionalMarginTop: 80,
                        minWidth: 1200
                    });
            });

            //BTN NAV-TRIGGER
            $('.btn-subnav').click(function() {
                $('.sidebar-nav').slideToggle('fast');
            });

            $('.list-videos').lightGallery({
                selector: '.play-button',
                youtubeThumbSize: 'maxresdefault',
                youtubePlayerParams: {
                    modestbranding: 1,
                    showinfo: 0,
                    rel: 0,
                    controls: 1
                }
            });
        </script>
        <?php include('general_footer.php'); ?>